import { Route, Routes } from 'react-router';
import { StateProvider } from './context/StateGlobal';
import Home from './pages/Home';
import Login from './pages/Login';

function App() {
  return (
    <StateProvider>
      <Routes>
        <Route exact path="/" element={ <Login /> } />
        <Route exact path="/home" element= {<Home />} />
      </Routes>
    </StateProvider>
  );
}

export default App;
