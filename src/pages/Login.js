import React from 'react';
import HelmetHead from '../components/HeaderHelmet';
import ToLogin from '../components/ToLogin';

function Login() {
  return (
    <div className='login-page'>
      <HelmetHead title="ioasys | Login" />
      <ToLogin />
    </div>
  );
}

export default Login;
