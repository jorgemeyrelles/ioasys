import React from 'react';
import Header from '../components/Header';
import HelmetHead from '../components/HeaderHelmet';
import ToHome from '../components/ToHome';

// import { Container } from './styles';

function Home() {
  return (
    <div className="home-one home-two">
      <HelmetHead title="ioasys | home" />
      <Header />
      <ToHome />
    </div>
  );
}

export default Home;