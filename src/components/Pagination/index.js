import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Pagination as ToPagination } from '@mui/material';
import PaginationItem from '@mui/material/PaginationItem';
import { useStyle } from './style';

function Pagination() {
  const style = useStyle();

  const location = useLocation();
  const query = new URLSearchParams(location.search);
  const page = parseInt(query.get('page') || '1', 10);
  return (
    <ToPagination
      class={style.pag}
      page={page}
      count={100}
      renderItem={(item) => (
        <PaginationItem
          component={Link}
          to={`/home/?page=${item.page}`}
          {...item}
        />
      )}
    />
  );
}

export default Pagination;
