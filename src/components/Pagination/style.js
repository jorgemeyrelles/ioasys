import { makeStyles } from '@material-ui/styles';

export const useStyle = makeStyles({
  pag: {
    right: 0,
    margin: '10px 40px 10px 0',
    position: 'fixed',
    top: 'calc(91% - 0.1em)',
  },
});
