import React, { useEffect, useState } from 'react';
// import { experimentalStyled as styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
// import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import CardBook from './CardBook';
import Pagination from './Pagination';
import { useLocation } from 'react-router';

// const Item = styled(Paper)(({ theme }) => ({
//   backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
//   ...theme.typography.body2,
//   padding: theme.spacing(1),
//   textAlign: 'center',
//   color: theme.palette.text.secondary,
// }));

function CardList() {
  const [books, setBooks] = useState([]);
  const local = localStorage.getItem('books');

  const location = useLocation();
  const query = new URLSearchParams(location.search);
  const page = query.get("page");

  useEffect(() => {
    if (local) return setBooks(JSON.parse(local));
  }, [page]);
  // console.log(books.data);

  return (
    <Box
      sx={{ flexGrow: 1 }}
      style={{ height: '500px', overflowY: 'scroll', overflowX: 'hidden' }}
    >
      <Grid
        direction="row"
        justifyContent="center"
        alignItems="center"
        container
        spacing={{ xs: 1, md: 1 }}
        columns={{ xs: 4, sm: 8, md: 16 }}
      >
        {books.length !== 0 && books.data.slice(0, 12).map((book, index) => (
          <Grid item xs={1} sm={4} md={4} key={index}>
            <CardBook book={book} />
          </Grid>
        ))}
      </Grid>
      <Pagination />
    </Box>
  );
}

export default CardList;