import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import logo from '../img/logoWhite.png';
import { getBooks, postSignIn } from '../service/Api';
import './styles/login.css';

function ToLogin() {
  const [loading, setLoading] = useState(true);
  const [login, setLogin] = useState({ email: '', password: '' });
  const [err, setErr] = useState(false);
  
  const navigate = useNavigate();

  function handleChange({ target: { value, name } }) {
    // modifica status do error
    setErr(false);
    // coleta valores do input
    setLogin({
      ...login,
      [name]: value,
    });
  }

  useEffect(() => {
    const { email, password } = login;
    const minPass = 6;
    const emailValid = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/;
    // valida formato do e-mail e tamanho da senha
    if (emailValid.test(email) && password.length >= minPass && password.length !== 0) {
      setLoading(false);
    } else {
      // alerta de erro
      setLoading(true);
    }
  }, [login]);

  const persist = (object) => {
    // const localTkn = localStorage.getItem('token');
    // const localUser = localStorage.getItem('user');
    const obj = object.headers;
    const refreshToken = Object.entries(obj)[Object.entries(obj).length - 1][1];
    // console.log(obj.authorization);
    const getAll = async (token) => {
      // console.log(page);
      const response = await getBooks(token, 1);
      console.log(response, 1);
      return localStorage.setItem('books', JSON.stringify(response.data));
    }
    getAll({ token: obj.authorization});
    localStorage.setItem('token', JSON.stringify(obj.authorization));
    localStorage.setItem('refresh-token', JSON.stringify(refreshToken));
    localStorage.setItem('user', JSON.stringify(object.data));
  };

  async function handleSubmit(e) {
    e.preventDefault();
    const { email, password } = login;
    try {
      const response = await postSignIn({ email, password: String(password) });
      setErr(false)
      await persist(response);
      navigate("/home/?page=1");
      return response; 
    } catch (error) {
      console.error('auth', error);
      setErr(true);
    }
  }
  return (
    <div className='form-position'>
      <div className='form-title'>
        <div className='image-form'>
          <img src={logo} alt="white logo" />
        </div>
        <span>Books</span>
      </div>
      <form onSubmit={(e) => handleSubmit(e)}>
        <label>
          <div className="input-label">Email</div>
          <div>
            <input
              className="input-email"
              type="email"
              onChange={handleChange}
              name="email"
            />
          </div>
        </label>
        <label>
          <div className="input-label">Senha</div>
          <div>
            <input
              className="input-email"
              type="password"
              onChange={handleChange}
              name="password"
            />
            <button
              className={loading ? "btn-entrar-disabled" : "btn-entrar"}
              type="submit"
              onClick={(e) => handleSubmit(e)}
              disabled={loading}
            >
              Entrar
            </button>
          </div>
        </label>
        {err && (
          <div>
            <div className="vetor"></div>
            <div className="back-error">
              <div className="error-text">E-mail / senha inválido(s)</div>
            </div>
          </div>
        )}
      </form>
    </div>
  );
}

export default ToLogin;