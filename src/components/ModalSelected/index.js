import React, { useContext } from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { StateContext } from '../../context/StateGlobal';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import { useStyle } from './styles';

function ModalSelected() {
  const { tkn, setTkn, user } = useContext(StateContext);
  const style = useStyle();

  const handleClose = () => setTkn(false);
  console.log(user);

  return (
    <div>
      <Modal
        open={tkn}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box className={style.box}>
          <Card sx={{ maxWidth: '100%', height: '100%' }}>
            <Card
              style={{ display: 'flex' }}
            >
              <CardMedia
                component="img"
                height="100%"
                tyle={{ width: '50%' }}
                image={user && user.imageUrl}
                alt={user && user.title}
                className={style.cardBook}
              />
              <CardContent style={{ width: '200%' }}>
                <Typography
                  class={style.stTitle}
                  gutterBottom
                  variant="h4"
                  component="div"
                >
                  {user && user.title}
                </Typography>
                <Typography
                  class={style.stAuthor}
                  variant="subtitle2"
                  color="text.secondary"
                >
                  {user && user.authors.join(', ')}
                </Typography>
                <Typography
                  class={style.inform}
                  variant="body2"
                  color="text.secondary"
                >
                  INFORMAÇÔES
                </Typography>
                <table>
                  <tbody>
                    <tr>
                      <td style={{ fontSize: '12px' }}>Páginas</td>
                      <td>
                        <Typography
                          class={style.pg}
                          variant="body2"
                          color="text.secondary"
                        >
                          {user && `${user.pageCount} páginas`}
                        </Typography>
                      </td>
                    </tr>
                    <tr>
                      <td style={{ fontSize: '12px' }}>Editora</td>
                      <td>
                        <Typography
                          class={style.stPublisher}
                          variant="body2"
                          color="text.secondary"
                        >
                          {user && `Editora ${user.publisher}`}
                        </Typography>
                      </td>
                    </tr>
                    <tr>
                      <td style={{ fontSize: '12px' }}>Publicação</td>
                      <td>
                        <Typography
                          class={style.stPublished}
                          variant="body2"
                          color="text.secondary"
                        >
                          {user && `Publicado em ${user.published}`}
                        </Typography>
                      </td>
                    </tr>
                    <tr>
                      <td style={{ fontSize: '12px' }}>Idioma</td>
                      <td>
                        <Typography
                          class={style.stPublished}
                          variant="body2"
                          color="text.secondary"
                        >
                          {user && user.language}
                        </Typography>
                      </td>
                    </tr>
                    <tr>
                      <td style={{ fontSize: '12px' }}>
                        {'isbn10'.toUpperCase()}
                      </td>
                      <td>
                        <Typography
                          class={style.stPublished}
                          variant="body2"
                          color="text.secondary"
                        >
                          {user && user.isbn10}
                        </Typography>
                      </td>
                    </tr>
                    <tr>
                      <td style={{ fontSize: '12px' }}>
                        {'isbn13'.toUpperCase()}
                      </td>
                      <td>
                        <Typography
                          class={style.stPublished}
                          variant="body2"
                          color="text.secondary"
                        >
                          {user && user.isbn13}
                        </Typography>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <Typography
                  class={style.inform}
                  variant="body2"
                  color="text.secondary"
                >
                  RESENHA DA EDITORA
                </Typography>
                <Typography
                  class={style.stDesc}
                  variant="body2"
                  color="text.secondary"
                >
                  {user && user.description}
                </Typography>
              </CardContent>
            </Card>
          </Card>
        </Box>
      </Modal>
    </div>
  );
}

export default ModalSelected;