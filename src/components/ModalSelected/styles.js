import { makeStyles } from '@material-ui/styles';

export const useStyle = makeStyles({
  box: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '60%',
    height: '70%',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    borderRadius: '10px',
    boxShadow: 24,
    p: 4,
  },

  cardBook: {
    transform: 'scale(0.7)',
    position: 'relative',
    top: '-20px',
  },

  stTitle: {
    lineHeight: '20px',
    fontFamily: 'Heebo',
    fontWeight: 500,
  },

  stAuthor: {
    marginBottom: '1px',
    lineHeight: '20px',
    fontSize: '12px',
    fontFamily: 'Heebo',
    color: '#AB2680',
  },

  inform: {
    marginTop: '50px 5px',
    lineHeight: '28px',
    fontSize: '12px',
    fontFamily: 'Heebo',
    fontWeight: 500,
    color: '#333',
  },

  pg: {
    marginTop: '20px 5px',
    lineHeight: '20px',
    fontSize: '12px',
    fontFamily: 'Heebo',
    color: '#999999',
  },

  stPublisher: {
    marginBottom: '0 5 px',
    lineHeight: '20px',
    fontSize: '12px',
    fontFamily: 'Heebo',
    color: '#999999',
  },

  stPublished: {
    marginBottom: '0 1px',
    lineHeight: '20px',
    fontSize: '12px',
    fontFamily: 'Heebo',
    color: '#999999',
  },

  stDesc: {
    marginBottom: '0 1px',
    height: '200px',
    lineHeight: '20px',
    fontSize: '12px',
    fontFamily: 'Heebo',
    color: '#999999',
    overflow: 'scroll',
    overflowX: 'hidden',
  },

});
