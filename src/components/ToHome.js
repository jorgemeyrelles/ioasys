import React, { useEffect } from 'react';
import { useLocation } from 'react-router';
import { getBooks } from '../service/Api';
import CardList from './CardList';
import ModalSelected from './ModalSelected';

function ToHome() {
  // const getAll = async ({ token }) => {
  //   const response = await getBooks({ token });
  //   console.log(response);
  // }
  const location = useLocation();
  const query = new URLSearchParams(location.search);
  const page = query.get("page");
  
  useEffect(() => {
    try {
      const getAll = async (token) => {
        // console.log(page);
        const response = await getBooks(token, page);
        // console.log(response, page);
        return localStorage.setItem('books', JSON.stringify(response.data));
      }
      getAll({ token: localStorage.getItem('token')});
    } catch (error) {
      console.error(error);
    }
  }, [page]);

  return (
    <div className="main-container">
      <ModalSelected />
      <CardList />
    </div>
  );
}

export default ToHome;