import React, { useContext, useMemo, useState } from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import { StateContext } from '../../context/StateGlobal';
import { useStyle } from './style';

function CardBook(props) {
  const [book, setBook] = useState([]);

  const style = useStyle();

  const { setTkn, setUser } = useContext(StateContext);
  const {
    imageUrl,
    title,
    authors,
    pageCount,
    publisher,
    published,
  } = props.book;
  // console.log(props.book);
  useMemo(() => {
    const res = JSON.parse(localStorage.getItem('books'));
    setBook(res);
  }, []);

  return (
    <Card sx={{ maxWidth: 250, maxHeight: 250 }}>
      <CardActionArea
        style={{ display: 'flex' }}
        onClick={() => {
          setTkn(true);
          setUser(props.book);
        }}
      >
        <CardMedia
          component="img"
          height="120px"
          tyle={{ width: '100%' }}
          image={book.data && imageUrl}
          alt={book.data && title}
          style={{ margin: '0 5px', padding: '5px 2px' }}
        />
        <CardContent style={{ padding: '2px', width: '200%' }}>
          <Typography
            style={{ marginBottom: '1px', lineHeight: '20px', fontSize: '14px', fontFamily: 'Heebo', fontWeight: 500 }}
            gutterBottom
            variant="h6"
            component="div"
          >
            {book.data && title}
          </Typography>
          <Typography
            class={style.stAuthor}
            // style={{ marginBottom: '1px', lineHeight: '20px', fontSize: '12px', fontFamily: 'Heebo', color: '#AB2680' }}
            variant="subtitle2"
            color="text.secondary"
          >
            {book.data && authors.join(', ')}
          </Typography>
          <Typography
            style={{ marginTop: '20px 5px', lineHeight: '20px', fontSize: '12px', fontFamily: 'Heebo', color: '#999999' }}
            variant="body2"
            color="text.secondary"
          >
            {book.data && `${pageCount} páginas`}
          </Typography>
          <Typography
            style={{ marginBottom: '0 5 px', lineHeight: '20px', fontSize: '12px', fontFamily: 'Heebo', color: '#999999' }}
            variant="body2"
            color="text.secondary"
          >
            {book.data && `Editora ${publisher}`}
          </Typography>
          <Typography
            style={{ marginBottom: '0 1px', lineHeight: '20px', fontSize: '12px', fontFamily: 'Heebo', color: '#999999' }}
            variant="body2"
            color="text.secondary"
          >
            {book.data && `Publicado em ${published}`}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

export default CardBook;