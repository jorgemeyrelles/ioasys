import { makeStyles } from '@material-ui/styles';

export const useStyle = makeStyles({
  stAuthor: {
    width: '150px',
    marginBottom: '1px',
    lineHeight: '20px',
    fontSize: '12px',
    fontFamily: 'Heebo',
    color: '#AB2680',
    textOverflow: 'ellipsis',
    overflowX: 'hidden',
    whiteSpace: 'nowrap',
  },

});
