import React, { createContext, useState } from 'react';
import PropTypes from 'prop-types';

export const StateContext = createContext();

export function StateProvider({ children }) {
  const [user, setUser] = useState(false);
  const [tkn, setTkn] = useState(false);
  const value = {
    user,
    setUser,
    tkn,
    setTkn,
  };

  return (
    <StateContext.Provider value={ value }>
      { children }  
    </StateContext.Provider>
  );
}

StateProvider.propTypes = {
  children: PropTypes.node,
}.isRequired;
