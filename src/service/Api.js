import axios from "axios";

const { REACT_APP_URL } = process.env;
const URL_LOGIN = '/auth/sign-in';
const URL_BOOKS = '/books/';

const postSignIn = async ({ email, password }) => {
  const respose = await axios.post(`${REACT_APP_URL}${URL_LOGIN}`, { email, password });
  return respose;
};

const getBooks = async ({ token }, page) => {
  // console.log(JSON.parse(token), page);
  const response = await axios.get(`${REACT_APP_URL}${URL_BOOKS}?page=${page}`,
    { headers: { Authorization: `Bearer ${JSON.parse(token)}` }});
  // console.log(response);
  return response;
};

const getBookById = async ({ token, id }) => {
  const response = await axios.get(`${REACT_APP_URL}${URL_BOOKS}/${id}`, { headers: { Authorization: token }});
  return response;
};

export { postSignIn, getBooks, getBookById };
